FROM busybox:glibc
LABEL MAINTAINER="yuki@coala.io"

ENV NOMAD_VERSION=0.8.6

RUN set -ex && \
    \
    mkdir -p /usr/local/bin && \
    \
    wget "https://releases.hashicorp.com/nomad/$NOMAD_VERSION/nomad_""$NOMAD_VERSION""_linux_amd64.zip" \
         -O /tmp/nomad.zip && \
    unzip /tmp/nomad.zip -d /tmp/ && \
    mv /tmp/nomad /usr/local/bin/nomad && \
    \
    rm -rf /tmp/*
